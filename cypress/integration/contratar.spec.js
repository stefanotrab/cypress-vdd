/// <reference types="cypress" />

import { generate } from 'gerador-validador-cpf';

describe('Fluxo de contratar plano', () => {
    it('montar plano', () => {
        // home
        // cy.visit('http://app-homolog.hapvida.com.br/contrateonline');
        // cy.visit('https://www.hapvida.com.br/contrateonline');
        cy.visit('http://localhost:4200');
        cy.get('.btn-primary').first().click();

        // dados-pessoais
        cy.get('#coberturaSelect > .mat-select-trigger > .mat-select-arrow-wrapper').click();
        cy.get('#mat-option-6 > .mat-option-text').click();
        cy.get('#paraQuantasPessoasSelect > .mat-select-trigger').click();
        cy.get('#mat-option-0 > .mat-option-text').click();
        cy.get('.mat-card-actions > .mat-focus-indicator').click();

        // contratar-online
        cy.get('#nome').focus().type('João Vitor Souza');
        cy.get('#mat-input-1').focus().type('joaovitorsouza@gmail.com');
        cy.get('#mat-input-2').focus().type('(85)99999-3333');
        cy.get('.mat-card-content > .mat-focus-indicator').click();

        // informacaoes-adicionais
        cy.get('.datalNascimento > .mat-figure > .outline-inputs-width > .mat-form-field-wrapper > .mat-form-field-flex').click();
        cy.get('.mat-calendar-period-button').click();
        cy.get('.mat-calendar-previous-button').click();
        cy.get('[aria-label="1989"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="março de 1989"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="21 de março de 1989"] > .mat-calendar-body-cell-content').click();
        cy.get(':nth-child(3) > .mat-focus-indicator > .mat-button-wrapper').click();

        // prateleira
        cy.get(':nth-child(2) > .card-plano-novo > [fxlayoutalign="center right"] > .mat-focus-indicator').click();

        // prateleira-detalhe
        cy.get('.div-card > .botao-contrate-plano-online').click();

        // termo-contratação
        cy.get('.botao-primary').click();

        // cadastro
        cy.get('[fxlayout="column"] > .botao-primary').click();

        // compra-stepper/0
        cy.get('[class=button-big-marked]').contains('Preencher Dados Pessoais').click();

        // cadastro-dados-pessoais/titular
        cy.get('#cpf').type(`'${generate()}'`);
        cy.get('#generoSelect > .mat-select-trigger').click();
        cy.get('mat-option').contains('Masculino').click();
        cy.get('#nomeCompletoMae').type('Maria Oliveira Paes');
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click();
        cy.get('mat-option').contains('SOLTEIRO').click();
        cy.get('#tipoDocumento > .mat-select-trigger').click();
        cy.get('mat-option').contains('Registro Geral com CPF').click();
        cy.get('#numeroDocumento').type('213611041');
        cy.get('#orgaoEmissor').type('SSP');
        cy.get('#ufSelecionada > .mat-select-trigger').click();
        cy.get('mat-option').contains('Ceará (CE)').click();
        cy.get('.button-adicionar').click();

        // foto-documentos/indentificacao/titular
        const photo = 'photo-test.jpg';
        cy.get('#inputFrente').attachFile(photo);
        cy.get('#inputVerso').attachFile(photo);
        cy.get(':nth-child(4) > .mat-focus-indicator').click();

        // cadastro-dados-pessoais/titular
        cy.get('div.ng-star-inserted > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get('[class=button-big-marked]').contains('Preencher Endereço').click();

        // cadastro-dados-endereco/titular
        cy.get('#cep').type('60192-275');
        cy.server();
        cy.route('GET', '**/recuperaDadosEndereco/**').as('getRecuperaDadosEndereco');
        cy.get('.example-card').click();
        cy.wait('@getRecuperaDadosEndereco');
        cy.get('#numero').focus().type('666');
        cy.get('#pontoReferencia').click();
        cy.get('#pontoReferencia').type('Igreja Universal do Reino de Deus');
        cy.get('#declaro1').click();
        cy.get('#declaro2').click();
        cy.get('#inputFrente').attachFile(photo);
        cy.get(':nth-child(3) > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get('#cdk-step-content-2-0 > .mat-stroked-button').click();

        // declaracao-saude
        cy.get('[fxlayout="column"] > .botao-primary').click();

        // carta-orientacao
        cy.get('input').click();
        cy.get('.mat-card-actions > .mat-focus-indicator').click();

        // compra-stepper/1
        // cy.get('#peso').click({force: true});
        cy.get('#peso').focus().type('81');
        // cy.get('#altura').click({force: true});
        cy.get('#altura').focus().type('178');

        cy.server();
        cy.route('GET', '**/saude').as('getSaude');
        cy.route('POST', '**/saude').as('postSaude');

        cy.get('#cdk-step-content-3-1 > .mat-stroked-button').click();

        cy.wait('@getSaude');
        cy.wait('@postSaude');

        // doencas-preexistentes
        cy.get('.botao-primary').click();
        cy.get('.botao-primary').contains('Confirmo').click({force: true});
    });
});