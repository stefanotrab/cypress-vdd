/// <reference types="cypress" />

import { generate } from 'gerador-validador-cpf';

describe('Fluxo de contratar plano', () => {
    it('montar plano', () => {
        // home
        // cy.visit('http://app-homolog.hapvida.com.br/contrateonline');
        // cy.visit('https://www.hapvida.com.br/contrateonline');
        cy.visit('http://localhost:4200');
        cy.get('.btn-primary').first().click();

        // dados-pessoais
        cy.get('#coberturaSelect > .mat-select-trigger > .mat-select-arrow-wrapper').click();
        cy.get('#mat-option-6 > .mat-option-text').click();
        cy.get('#paraQuantasPessoasSelect > .mat-select-trigger').click();
        cy.get('#mat-option-2 > .mat-option-text').click();
        cy.get('.mat-card-actions > .mat-focus-indicator').click();

        // contratar-online
        cy.get('#nome').focus().type('Pedro Oliveira Paes');
        cy.get('#mat-input-1').focus().type('pedro.oliveira@gmail.com');
        cy.get('#mat-input-2').focus().type('(85)99999-9999');
        cy.get('.mat-card-content > .mat-focus-indicator').click();

        // informacaoes-adicionais
        cy.get('#dateOfBirth').click();
        cy.get('.mat-calendar-period-button').click();
        cy.get('.mat-calendar-previous-button').click();
        cy.get('[aria-label="1989"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="março de 1989"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="21 de março de 1989"] > .mat-calendar-body-cell-content').click();
        cy.get(':nth-child(3) > .mat-focus-indicator > .mat-button-wrapper').click();

        cy.get('[data-index="0"] > .card-info-dependente > .mat-grid-list > :nth-child(1) > .datalNascimento > .mat-figure > .outline-inputs-width > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-suffix > .mat-datepicker-toggle > .mat-focus-indicator > .mat-button-wrapper > .mat-icon').click();
        cy.get('.mat-calendar-period-button').click();
        cy.get('.mat-calendar-previous-button').click();
        cy.get('[aria-label="1992"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="maio de 1992"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="3 de maio de 1992"] > .mat-calendar-body-cell-content').click();
        cy.get(':nth-child(3) > .mat-focus-indicator > .mat-button-wrapper').click();
        cy.get('#parentescoSelect0 > .mat-select-trigger').click();
        cy.get('#mat-option-108 > .mat-option-text').click();


        cy.get('[data-index="1"] > .card-info-dependente > .mat-grid-list > :nth-child(1) > .datalNascimento > .mat-figure > .outline-inputs-width > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-suffix > .mat-datepicker-toggle > .mat-focus-indicator > .mat-button-wrapper > .mat-icon').click();
        cy.get('.mat-calendar-period-button').click();
        cy.get('[aria-label="2012"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="dezembro de 2012"] > .mat-calendar-body-cell-content').click();
        cy.get('[aria-label="14 de dezembro de 2012"] > .mat-calendar-body-cell-content').click();
        cy.get('#parentescoSelect1 > .mat-select-trigger').click();
        cy.get('#mat-option-125 > .mat-option-text').click();
        cy.get(':nth-child(3) > .mat-focus-indicator > .mat-button-wrapper').click();

        // prateleira
        cy.get(':nth-child(2) > .card-plano-novo > [fxlayoutalign="center right"] > .mat-focus-indicator').click();

        // prateleira-detalhe
        cy.get('.div-card > .botao-contrate-plano-online').click();

        // termo-contratação
        cy.get('.botao-primary').click();

        // cadastro
        cy.get('[fxlayout="column"] > .botao-primary').click();

        // compra-stepper/0
        cy.get('[class=button-big-marked]').contains('Preencher Dados Pessoais').click();

        // cadastro-dados-pessoais/titular
        cy.get('#cpf').type(`'${generate()}'`);
        cy.get('#generoSelect > .mat-select-trigger').click();
        cy.get('mat-option').contains('Masculino').click();
        cy.get('#nomeCompletoMae').type('Maria Oliveira Paes');
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click();
        cy.get('mat-option').contains('CASADO').click();
        cy.get('#tipoDocumento > .mat-select-trigger').click();
        cy.get('mat-option').contains('Registro Geral com CPF').click();
        cy.get('#numeroDocumento').type('338868495');
        cy.get('#orgaoEmissor').type('SSP');
        cy.get('#ufSelecionada > .mat-select-trigger').click();
        cy.get('mat-option').contains('Ceará (CE)').click();
        cy.get('.button-adicionar').click();

        // foto-documentos/indentificacao/titular
        const photo = 'photo-test.jpg';
        cy.get('#inputFrente').attachFile(photo);
        cy.get('#inputVerso').attachFile(photo);
        cy.get(':nth-child(4) > .mat-focus-indicator').click();

        // cadastro-dados-pessoais
        cy.get('div.ng-star-inserted > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get('[class=button-big-marked]').contains('Preencher Endereço').click();

        // cadastro-dados-endereco/titular
        cy.get('#cep').type('60192275');
        cy.server();
        cy.route('GET', '**/recuperaDadosEndereco/**').as('getRecuperaDadosEndereco');
        cy.get('#numero').click();
        cy.wait('@getRecuperaDadosEndereco');
        cy.get('#numero').focus().type('666');
        cy.get('#pontoReferencia').click();
        cy.get('#pontoReferencia').type('Igreja Universal do Reino de Deus');
        cy.get('#declaro1').click();
        cy.get('#declaro2').click();
        cy.get('#inputFrente').attachFile(photo);
        cy.get(':nth-child(3) > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get(':nth-child(2) > .mat-grid-list > :nth-child(1) > [style="left: 0px; width: calc((100% - 0px) * 1 + 0px); top: calc(48px); height: calc(47px);"] > .mat-figure > .button-big-marked').contains('Preencher Dados Pessoais').click();

        // cadastro-dados-pessoais/titular
        cy.get('#nomeCompleto').type('Joana Oliveira Paes');
        cy.get('#cpf').type(`'${generate()}'`);
        cy.get('#generoSelect > .mat-select-trigger').click();
        cy.get('mat-option').contains('Feminino').click();
        cy.get('#nomeCompletoMae').type('Francisca Pereira Fialho');
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click();
        cy.get('mat-option').contains('CASADO').click();
        cy.get('#email').type('joana@gmail.com');
        cy.get('#telefone').type('(85)989898989');
        cy.get('#tipoDocumento > .mat-select-trigger').click();
        cy.get('mat-option').contains('Registro Geral com CPF').click();
        cy.get('#numeroDocumento').type('500691253');
        cy.get('#orgaoEmissor').type('SSP');
        cy.get('#ufSelecionada > .mat-select-trigger').click();
        cy.get('mat-option').contains('Ceará (CE)').click();
        cy.get('.button-adicionar').click();

        // foto-documentos/indentificacao/titular
        cy.get('#inputFrente').attachFile(photo);
        cy.get('#inputVerso').attachFile(photo);
        cy.get(':nth-child(4) > .mat-focus-indicator').click();

        //cadastro-dados-pessoais/0
        cy.get('div.ng-star-inserted > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get(':nth-child(3) > .mat-grid-list > :nth-child(1) > [style="left: 0px; width: calc((100% - 0px) * 1 + 0px); top: calc(48px); height: calc(47px);"] > .mat-figure > .button-big-marked').contains('Preencher Dados Pessoais').click();

        // cadastro-dados-pessoais/titular
        cy.get('#nomeCompleto').type('Yan Pedro Oliveira Paes');
        cy.get('#generoSelect > .mat-select-trigger').click();
        cy.get('mat-option').contains('Masculino').click();
        cy.get('#nomeCompletoMae').type('Joana Oliveira Paes');
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click();
        cy.get('mat-option').contains('SOLTEIRO').click();
        cy.get('#tipoDocumento > .mat-select-trigger').click();
        cy.get('mat-option').contains('Certidao do nascimento').click();
        cy.get('#numeroDocumento').type(`'${generate()}'`);
        cy.get('#orgaoEmissor').type('SSP');
        cy.get('#ufSelecionada > .mat-select-trigger').click();
        cy.get('mat-option').contains('Ceará (CE)').click();
        cy.get('.button-adicionar').click();

        // foto-documentos/indentificacao/titular
        cy.get('#inputFrente').attachFile(photo);
        cy.get('#inputVerso').attachFile(photo);
        cy.get(':nth-child(4) > .mat-focus-indicator').click();

        // cadastro-dados-pessoais/1
        cy.get('div.ng-star-inserted > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get('#cdk-step-content-4-0 > .mat-stroked-button').click();

        // declaracao-saude
        cy.get('[fxlayout="column"] > .botao-primary').click();

        // carta-orientacao
        cy.get('input').click();
        cy.get('.mat-card-actions > .mat-focus-indicator').click();

        // compra-stepper/1
        cy.get('#peso').focus().type('81');
        cy.get('#altura').focus().type('178');
        cy.get('#peso0').focus().type('60');
        cy.get('#altura0').focus().type('162');
        cy.get('#peso1').focus().type('25');
        cy.get('#altura1').focus().type('130');

        cy.server();
        cy.route('GET', '**/saude').as('getSaude');
        cy.route('POST', '**/saude').as('postSaude');

        cy.get('#cdk-step-content-5-1 > .mat-stroked-button').click();

        cy.wait('@getSaude');
        cy.wait('@postSaude');

        // doencas-preexistentes
        cy.get('.botao-primary').click();
        cy.get('.botao-primary').contains('Confirmo').click({force: true});
    });
});